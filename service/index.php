<?php
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../vendor/zeedhi/framework/bootstrap.php';

$instanceManager = \Zeedhi\Framework\DependencyInjection\InstanceManager::getInstance();
$instanceManager->loadFromFile(__DIR__.'/../environment.xml');
$instanceManager->loadFromFile(__DIR__.'/../config/services.xml');
$instanceManager->compile();

/** @var \Zeedhi\Framework\Application $app */
$app = $instanceManager->getService('application');

$app->run();