<?php
namespace Zeedhi\TreinoImagens\Controller;

use Zeedhi\Framework\Controller\Crud;
use Zeedhi\Framework\DTO;
use Zeedhi\Framework\DataSource\Manager;
use Zeedhi\TreinoImagens\Exception\Image as ImageException;
use Zeedhi\Framework\DataSource\DataSet;

class Image extends Crud {
    protected $dataSourceName = 'image';
    protected $session;

    public function __construct(Manager $dataSourceManager){
        parent::__construct($dataSourceManager);
    }
    
}