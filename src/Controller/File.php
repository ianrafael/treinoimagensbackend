<?php
namespace Zeedhi\TreinoImagens\Controller;

use Zeedhi\Framework\DTO\Response\Message;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DTO\Request;
use Zeedhi\TreinoImagens\Service\File as FileService;
use Zeedhi\TreinoImagens\Exception\File as FileException;
use Zeedhi\Framework\Remote\Server as Server;
use Zeedhi\Framework\DataSource\DataSet;
use Zeedhi\Framework\DTO\Response\Error;

class File extends Server{

    protected $fileService;
    protected $fileServerURL;
    protected $zhFileUploader;
    protected $fileServerString;
    protected $customFolders=array();

    public function __construct (FileService $fileService, $fileServerURL, $zhFileUploader, $fileServerString, $uploadRoutes){
        $this->fileService = $fileService;
        $this->fileServerURL = $fileServerURL;
        $this->zhFileUploader = $zhFileUploader;
        $this->fileServerString = $fileServerString;
        $this->customFolders = array(
            'IMAGE'         => $uploadRoutes['/uploadimage']['customFolder'],
            'ANNOTATION'    => "TranningSet/Annotations"
        );
    }

    public function uploadImageFile (Request $request, Response $response){
        try{
            $data = $request->getRow();
            $insertDate = date('Y-m-d H:i:s');
            $lastModification = $insertDate;
            $productImages = $data['productImage'];
            $nrorg = $data['NRORG'];

            $mapfunc = function ($array){return $this->fileServerURL.$array['path'];};
            $imagesUrl = array_map($mapfunc, $productImages);

            $params = array(
                'CDITEM'        => $data['CDITEM'],
                'DTINCLUSAO'    => $insertDate,
                'DTULTATU'      => $lastModification,
                'ITEMIMAGEMURL' => $imagesUrl,
                'NRORG'         => $nrorg
            );

            $imageStats = $this->fileService->addItemImage($params);
            $return = array(
                'status'=>true
            );

            if(isset($data['origin']) && $data['origin'] == "webcam"){
                $imageUrl = array('ITEMIMAGEMURL' => $imagesUrl[0]);
                $base64image = $this->getImageBase64Function($imageUrl);
                $lastRow = $this->fileService->getLastRow();
                $return = array (
                    'base64image'   => $base64image['base64image'],
                    'CDIMAGEMITEM'  => $lastRow[0]['CDIMAGEMITEM'],
                    'status'        => true
                );
            }
            $response->addDataSet(new DataSet('response', $return));
        }catch(\Exception $e){
            $message = 'Erro ao subir imagem. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }

    public function uploadAnnotationFile(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $annotationData = $data['imageData'];
            $rowFile = $this->generateXMLFile($annotationData);
            if(!isset($rowFile) && $rowFile['status'] == false){
                throw new \Exception($rowFile['message']);
            }
            $fileStats = $this->postFileServer($rowFile['rowFile']);
            $annotationFilePath = $this->fileServerURL.$fileStats['UPLOADCOMMENT'][0]['path'];
            $fileStats['UPLOADCOMMENT'][0]['path'] = $annotationFilePath;
            $annotationDataFile = array(
                            'fileStats' => $fileStats['UPLOADCOMMENT']
            );
            $labelPosArray = array(
                "xmin"  => $annotationData['xmin'],
                "ymin"  => $annotationData['ymin'],
                "xmax"  => $annotationData['xmax'],
                "ymax"  => $annotationData['ymax']
            );
            $labelPos = json_encode($labelPosArray);
            $annotationUrl = $annotationDataFile['fileStats'][0]['path'];
            $lastModification = date('Y-m-d H:i:s');
            $imageid = $data['CDIMAGEMITEM'];
            $nrorg = $data['NRORG'];

            $params = array(
                'CDIMAGEMITEM'  => $imageid,
                'DTULTATU'      => $lastModification,
                'ITEMANOTURL'   => $annotationUrl,
                'NRORG'         => $nrorg,
                'LABELPOS'      => $labelPos
            );

            $path = $this->fileService->getFilesPath($params);
            if(isset($path) && $path['error'] == true){
                throw new \Exception($path['message']);
            }
            $annotationPath = $path['data']['ITEMANOTURL'];
            if(isset($annotationPath)){
                $this->deleteFile('ANNOTATION',$annotationPath);
            }
            $data = $this->fileService->addItemAnnotation($params);
            $response->addDataSet(new DataSet('response', $data));
        }catch(\Exception $e){
            $message = 'Erro ao adicionar anotação. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }

    public function deleteFile($fileType,$filePath){
        $splitedFilePath = explode('/', $filePath);
        $fileName = $splitedFilePath[count($splitedFilePath)-1];
        $fileRelativePath = "/".$this->customFolders[$fileType]."/".$fileName;
        $result = $this->zhFileUploader->deleteFile($fileRelativePath);
    }
    public function  deleteImage(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $cdimage = $data['CDIMAGEMITEM'];
            $nrorg = $data['NRORG'];
            $params = array(
                'CDIMAGEMITEM'  => $cdimage,
                'NRORG'         => $nrorg
            );
            $paths = $this->fileService->getFilesPath($params);
            if(isset($paths) && $paths['error'] == true){
                throw new \Exception($paths['message']);
            }
            $paths = $paths['data'];
            $imagePath = $paths['ITEMIMAGEMURL'];
            $annotationPath = $paths['ITEMANOTURL'];
            if(isset($imagePath)){
                $this->deleteFile('IMAGE',$imagePath);
            }
            if(isset($annotationPath)){
                $this->deleteFile('ANNOTATION',$annotationPath);
            }
            $result = $this->fileService->deleteImage($params);
            $response->addDataSet(new DataSet('response', $result));
        }catch(\Exception $e){
            $message = 'Erro ao deletar imagem. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }

    public function getImageBase64(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $imageBase64 = $this->getImageBase64Function($data);
            if($imageBase64['status']=false){
                 throw new \Exception($imageBase64['message']);
            }
            $response->addDataSet(new DataSet('response', $imageBase64));
        }catch(\Exception $e){
            $message = 'Erro ao gerar string base64 da imagem. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }

    public function getImageBase64Function($data){
        try{
            $imagesUrl = $data['ITEMIMAGEMURL'];
            $img = file_get_contents($imagesUrl);
            $result = base64_encode($img);
            $return = array(
                'base64image'   => $result,
                'status'        => true
            );
            return $return;
        }catch(\Exception $e){
            $return = array(
                'message'   => $e->getMessage(),
                'status'    => false
            );
            return $return;
        }
    }

    public function createTable(Request $request, Response $response){
        $response = $this->fileService->createTable();
        echo($response);
    }

    public function generateXMLFile($data){
        try{
            $imageData = $data;
            $annotationTemplate = $this->fileService->getXMLTemplate();
            $annotationTemplate = $this->fileService->rplcAnnotationParameters($imageData, $annotationTemplate);
            $annotationFile = "annotation.xml";
            $xmlFileBase64 = "application/xml;base64,".base64_encode($annotationTemplate);
            $rowFile = array(
                'b64File'       => $xmlFileBase64,
                'name'          => $annotationFile,
                'type'          => "application/xml",
                "size"          => ((strlen($xmlFileBase64) * (3/4)) - 1)/1000,
                "customFolders" => $this->customFolders['ANNOTATION']
            );
            $response = array(
                'rowFile'   => $rowFile,
                'status'    => true
            );
            return $response;
        }catch(\Exception $e){
            $response = array(
                'message'   => 'Erro ao gerar anotação. Erro: '.$e->getMessage(),
                'status'    => false
            );
            return $response;
        }
    }

    private function postFileServer($rowFile){
        $fileRow = array(
            'UPLOADCOMMENT' => array(
                array(
                    'b64File'           => $rowFile['b64File'],
                    'name'              => $rowFile['name'],
                    'type'              => $rowFile['type'],
                    'webkitRelativePath'=> '',
                    'size'              => $rowFile['size']
                )
            )
        );

        $this->zhFileUploader->uploadFilesInRow($fileRow, 'UPLOADCOMMENT' , $rowFile['customFolders']);
        return $fileRow;
    }

    public function getFileBase64(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $imageBase64 = $this->getFileBase64Function($data);
            $response->addDataSet(new DataSet('response', $imageBase64));
        }catch(\Exception $e){
            $message = 'Erro ao gerar string base64 da imagem. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }

    public function getFileBase64Function($data){
        try{
            $fileUrl = $data['url'];
            $file = file_get_contents($fileUrl);
            $result = base64_encode($file);
            $return = array(
                'base64file'   => $result,
                'status'        => true
            );
            return $return;
        }catch(\Exception $e){
            $return = array(
                'message'   => $e->getMessage(),
                'status'    => false
            );
            return $return;
        }
    }
    public function uploadTranningData(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $nrorg = $data['NRORG'];
            $tranningData = $data['tranningData'];
            var_dump($data);
            $return = array(
                'base64file'    => $result,
                'status'        => true
            );
            //return $return;
        }catch(\Exception $e){
            $return = array(
                'message'   => $e->getMessage(),
                'status'    => false
            );
            //return $return;
        }
    }
}