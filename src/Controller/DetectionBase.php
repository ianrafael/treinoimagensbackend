<?php
namespace Zeedhi\TreinoImagens\Controller;

use Zeedhi\Framework\DTO\Response\Message;
use Zeedhi\Framework\DTO\Response;
use Zeedhi\Framework\DTO\Request;
use Zeedhi\Framework\Remote\Server as Server;
use Zeedhi\Framework\DataSource\DataSet as DataSet;
use Zeedhi\TreinoImagens\Service\DetectionBase as DetectionBaseService;
use Zeedhi\Framework\DTO\Response\Error as Error;

class DetectionBase extends Server{

    protected $fileServerURL;
    protected $zhFileUploader;
    protected $fileServerString;
    protected $baseService;
    protected $customFolders;

    public function __construct (DetectionBaseService $baseService ,$fileServerURL, $zhFileUploader, $fileServerString){
        $this->baseService = $baseService;
        $this->fileServerURL = $fileServerURL;
        $this->zhFileUploader = $zhFileUploader;
        $this->fileServerString = $fileServerString;
        $this->customFolders = array(
            'BASE'         => "DetectionBaseData"
        );
    }

    public function uploadBase(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $nrorg = $data['NRORG'];
            $tranningData = $data['tranningData'][0];
            $fileUrl = $this->fileServerURL.$tranningData['path'];
            $insertDate = date('Y-m-d H:i:s');
            $lastModification = $insertDate;

            $params = array(
                'DTINCLUSAO'    => $insertDate,
                'DTULTATU'      => $lastModification,
                'BASEURL'       => $fileUrl,
                'NRORG'         => $nrorg
            );
            $baseStats = $this->baseService->insertBase($params);
            $return = array(
                'status'        => true
            );
            $response->addDataSet(new DataSet('response', $return));
        }catch(\Exception $e){
            $return = array(
                'message'   => $e->getMessage(),
                'status'    => false
            );
            $response->addDataSet(new DataSet('response', $return));
        }
    }
    public function selectBases(Request $request, Response $response){
        $return = $this->baseService->selectBases();
        $response->addDataSet(new DataSet('response', $return));
    }
    public function createTable(Request $request, Response $response){
        $return = $this->baseService->createTable();
        $response->addDataSet(new DataSet('response', $return));
    }
    public function  deleteImage(Request $request, Response $response){
        try{
            $data = $request->getRow();
            $cdbase = $data['CDBASETREINO'];
            $nrorg = $data['NRORG'];
            $params = array(
                'CDBASETREINO'  => $cdbase,
                'NRORG'         => $nrorg
            );
            $paths = $this->baseService->getFilesPath($params);
            if(isset($paths) && $paths['error'] == true){
                throw new \Exception($paths['message']);
            }
            $paths = $paths['data'];
            $basePath = $paths['BASEURL'];
            if(isset($basePath)){
                $this->deleteFile('BASE',$basePath);
            }
            $result = $this->baseService->deleteImage($params);
            $response->addDataSet(new DataSet('response', $result));
        }catch(\Exception $e){
            $message = 'Erro ao deletar imagem. Erro: '.$e->getMessage();
            $response->setError(new Error($message, 500));
        }
    }
    public function deleteFile($fileType,$filePath){
        $splitedFilePath = explode('/', $filePath);
        $fileName = $splitedFilePath[count($splitedFilePath)-1];
        $fileRelativePath = "/".$this->customFolders[$fileType]."/".$fileName;
        $result = $this->zhFileUploader->deleteFile($fileRelativePath);
    }
}